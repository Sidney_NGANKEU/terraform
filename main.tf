### provider #####

provider "aws" {}

### My variables #####

variable env_prefix {}
variable avail_zone {}
variable vpc_cidr_bloc {}
variable subnet_cidr_bloc {}
variable my_ip {}
variable public_key_location {}
variable instance_type {}

### aws virtual private cloud #####

resource "aws_vpc" "infra_vpc" {
  cidr_block       = var.vpc_cidr_bloc

  tags = {
    Name = "${var.env_prefix}-infra_vpc"
  }
}

### infra_subnet ####

resource "aws_subnet" "infra_subnet" {
  vpc_id     = aws_vpc.infra_vpc.id
  cidr_block = var.subnet_cidr_bloc
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.env_prefix}-infra_subnet"
  }
}

### security_group ####

resource "aws_security_group" "infra_security_group" {
  vpc_id      = aws_vpc.infra_vpc.id

    ingress {
    from_port = 22
    to_port = 22
    protocol = "TCP"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }


  tags = {
    Name = "${var.env_prefix}-infra_security_group"
  }
}

### internet_gateway ####

resource "aws_internet_gateway" "infra_igw" {
  vpc_id = aws_vpc.infra_vpc.id

  tags = {
    Name = "${var.env_prefix}-infra_internet_gateway"
  }
}

### route table #####

resource "aws_route_table" "infra_rtb" {
  vpc_id = aws_vpc.infra_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.infra_igw.id
  }

  tags = {
    Name = "${var.env_prefix}-infra_rtb"
  }
}

### route table association #####

resource "aws_route_table_association" "infra_rtb_asso" {
  subnet_id      = aws_subnet.infra_subnet.id
  route_table_id = aws_route_table.infra_rtb.id
}

### aws ami #####

data "aws_ami" "infra_OS" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

### keys pairs #####

resource "aws_key_pair" "ssh-key" {
  key_name = "server-key"
  public_key = file(var.public_key_location)
}

### EC2 instance #####

resource "aws_instance" "infra_EC2" {
  ami           = data.aws_ami.infra_OS.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.infra_subnet.id
  vpc_security_group_ids = [aws_security_group.infra_security_group.id]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name



  user_data = file("entry-script.sh")
  user_data_replace_on_change = true
  tags = {
    Name = "${var.env_prefix}-infra_EC2"
  }
}

output "my-ami" {
  value = data.aws_ami.infra_OS.id
}

output "EC2-publicIP" {
  value = aws_instance.infra_EC2.public_ip
}
